export interface ITaskItem {
    content: string;
    isDone: boolean;
}
